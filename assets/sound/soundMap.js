var soundMap = {
	jump0: ['assets/sound/jump0.mp3', 'assets/sound/jump0.ogg', 'assets/sound/jump0.wav'],
	bgmusic: ['assets/sound/bgmusic.mp3', 'assets/sound/bgmusic.ogg', 'assets/sound/bgmusic.wav'],
	dead: ['assets/sound/dead.mp3', 'assets/sound/dead.ogg', 'assets/sound/dead.wav'],
	sssnake: ['assets/sound/sssnake.mp3', 'assets/sound/sssnake.ogg', 'assets/sound/sssnake.wav'],
};