#!/usr/bin/python

import os
import re

indexFile = open('index.html', 'w')


header = """<!DOCTYPE html>
<html>
  <head>
    <title>Snakes and Letters</title>
    <meta property="og:title" content="Snakes and Letters"/>
    <meta property="og:image" content="http://jellenauta.com/games/LudumDarePostCompo/LD37/scrshot.png"/>
    <meta property="og:site_name" content="Games by Jelle & Anneroos"/> 
    <meta property="og:description" content="A hardcore 1.5-player platformer in which you climb to the top of a pyramid to reach the tomb of your ancestors. Along the way there are three relics to collect."/>
    <meta property="og:url" content="http://jellenauta.com/games/LudumDarePostCompo/LD37/"/>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <link rel="stylesheet" type="text/css" href="stylesheet.css" />
    <!-- font -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favoicon.png"> 
    <link href="https://fonts.googleapis.com/css?family=Alegreya+SC|Bungee+Outline|Cinzel" rel="stylesheet">
    <!--   <meta name="viewport"   content="width=device-width, user-scalable=no, initial-scale=1.0" /> -->
    <script src="bower_components/crafty/dist/crafty.js"></script>"""
footer = """    <script>
      window.addEventListener('load', Game.start);
    </script>
    
    <!--  Thumbnail voor Facebook enzo. ;) -->
    <link rel="image_src" href="screenshot.png"/>  
  </head>
  <body>
        
        
    <div id="canvascontainer">  
    <div id="cr-stage"></div>
    </div>
    <!--<div><p><h1> Title </h1></p></div>-->
    <div>
      <p>Made by Jelle & Anneroos with the <a href="http://craftyjs.com">Crafty library</a>. <br>
      Post-compo for <a href= "http://jellenauta.com/games/LudumDare/LD37">our entry</a> for <a href="http://www.ludumdare.com/compo/ludum-dare-37/?action=preview&uid=18490">Ludum Dare 37</a> with the theme 'One Room'.<br>
      Have a look at  <a href="http://jellenauta.com/games/">our other games</a>.</p> 
    </div>
    
  </body>
</html>"""

try:
  indexFile.write(header)
  jsRegex = re.compile('.*\.js');
  for root, dirs, files in os.walk('./src/'):
    for name in files:
      if jsRegex.match(name):
        indexFile.write('    <script src="')
        indexFile.write(os.path.join(root, name))
        indexFile.write('"></script>\n')
  for root, dirs, files in os.walk('./assets/'):
    for name in files:
      if jsRegex.match(name):
        indexFile.write('    <script src="')
        indexFile.write(os.path.join(root, name))
        indexFile.write('"></script>\n')

  indexFile.write(footer)

finally:
  indexFile.close()
