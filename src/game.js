var mutemusic = false;//false;
var mutesound = false;
var debug = false;//true;
var playing = false;
var fontFamily1  = "Alegreya SC"; // explanation text 
var fontFamily2 = "Cinzel"; // help text
var fontFamily3 = "Bungee Outline"; // title?
var mouseButtons = {left: 0, middle: 1, right: 2};
var sculptureIdx = 0;
var playerScore = 0;


// # Choices per layers of cake
	nrOfChoices = 5;

function saveState(state) {
	window.localStorage.setItem("gameState", JSON.stringify(state));
}
		 
function restoreState() {
	var state = window.localStorage.getItem("gameState");
	if (state) {
		return JSON.parse(state);
	} else {
		return null;
	}
}

if(restoreState()){
	
	var dummyvar = restoreState();
	if(dummyvar['highscore']){
		highscore = dummyvar['highscore'];
	}
}


function polylineToXYWH(x, y, polyline) {
    var minX = maxX = polyline[polyline.length - 1].x;
    var minY = maxY = polyline[polyline.length - 1].y;
    // calculate width and height of polyline
    for (var i = polyline.length - 2; i >= 0; i--) {
      var pl = polyline[i];
      if (pl.x < minX) {
        minX = pl.x;
      } else if (pl.x > maxX) {
        maxX = pl.x;
      }
      if (pl.y < minY) {
        minY = pl.y;
      } else if (pl.y > maxY) {
        maxY = pl.y;
      }
    }
    output = [x + minX, y + minY, maxX - minX,  maxY - minY]; // x, y, w, h
    return output
  }


Crafty.paths({
	audio: 'assets/sound/',
	images: 'assets/images/'
});

Game = {
	width: function() {
		return 800;
	},

	height: function() {
		return 600;
	},

    
    
	start: function() {
		
		
		// Start crafty and set a background color so that we can see it's working
		Crafty.init(Game.width(),Game.height(), 'cr-stage');
		Crafty.canvasLayer.init();
    //Crafty.viewport.zoom(20,0,0,100);
		ctx = Crafty.canvasLayer.context;
    //Crafty.pixelart(true);
		Crafty.scene('Loading');
	},
};