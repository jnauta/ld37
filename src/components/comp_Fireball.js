Crafty.c("Fireball", {
	init: function(){
		this.requires("2D, Canvas, OriginCoordinates,  BulletMoving, Collision, fireball, SpriteAnimation");
		//this.image(imageMap["shooter"]);
		this.z = params.zLevels["enemies"];
		//this.collision(new Crafty.polygon(10, 10, 30, 10, 30, 30, 10, 30));
		this._BulletMoving();
		// this.reel("Fire",500,0,0,2).animate("Fire",-1);
	},

	_Fireball: function(x, y, target, owner) {
		// this.h = this.w = 20;
		this.x = x - this.w / 2;
		this.y = y - this.h / 2;
		this.target = target;
		this.origin("center");
		var bulletDx = this.target.originX() - this.originX();
		var bulletDy = this.target.originY() - this.originY();
		var angle = Math.atan2(bulletDy, bulletDx);
		this.vx = Math.cos(angle) * params.vBullet;
		this.vy = Math.sin(angle) * params.vBullet;
		this.owner = owner;
		//this.target = target;
		
		// this.w = Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		//this.rotation = rotation;
		this.bind("EnterFrame", function() {
			if (this.hit("Platform")) {
				this.destroy();
			};
			if (this.hit("Player")) {
				respawnPlayer();//player.destroy();
			}
			var shootersHit = this.hit("Shooter");
			if (shootersHit) {
				if (this.owner.target === shootersHit[0].obj) {
					this.owner.target = null;
				}
				shootersHit[0].obj.stun();
				this.destroy();
			} else {
				var batsHit = this.hit('Bouncer');
				if (batsHit) { 
					if (this.owner.target === batsHit[0].obj) {
						this.owner.target = null;
					}
					batsHit[0].obj.destroy();
				} else {
					var minionsHit = this.hit("Minion");
					if (minionsHit) {
						if (this.owner.target === minionsHit[0].obj) {
							this.owner.target = null;
						}
						minionsHit[0].obj.destroy();
					}
				}
			} 

		});
		return this;
	},

	moveCollisionTest: function(dimension) {
		var platformHits = this.hit('Platform');
		if (platformHits) {
			this.destroy();
		}
		return false;
	},

});