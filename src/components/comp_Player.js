Crafty.c('SelectionRange', {
	init: function() {
		this.requires('2D, Canvas, Tween, Delay');
		this.bind("Draw", this.drawCircle);
        this.ready = true;   
        this.win = false;     
        this.outlineCircle = false;
        
        this.rangeColor = mycolors.selectionRange;
        this.rangeColorOutline = mycolors.selectionRangeOutline;
        this.rangeAlpha = 0.08 ;

        
 

        
	},

	_SelectionRange: function(x, y) {
		
		

		this.range = params.selectionRange;
		this.x = x;
		this.y = y;
		this.z = params.zLevels["background"]+1;
		// this.z = params.zLevels["player"]-1;
		this.w = this.h = 2 * params.selectionRange;
		this.rangeCircle = new Crafty.circle(this._x, this._y, this.range);
		this.attach(this.rangeCircle);

		this.bind("EnterFrame", function(){
			var allNodes = Crafty("Node");
			for (var i = allNodes.length - 1; i >= 0; i--) {
				var n  = Crafty(allNodes[i]);
				if (n.color === 0 || // green always goes
					(n.color === 1 && player.scepter.visible) || // red requires scepter
					(n.color === 2 && player.amulet.visible) || // black requires amulet
					(n.color === 5 && player.redSnake)) { // if red snake is active, we may shoot at a target in range
					if (this.rangeCircle.containsPoint(n.originX(), n.originY())){
						n.selectableFun(true);
						n.alpha = 1;
					} else {
						n.selectableFun(false);						
						if (n.color !== 1) { // red persists
							n.deselect();
							if(n === player.firstNode) {
								player.firstNode = null;
							}
						}
					}
				} else {
					n.selectableFun(false);
				}
			}
		});
		return this;
	},

	drawCircle: function(e) {
		//console.log('drawing at ' + this.x + ", " + this.y);
        var ctx = e.ctx;
        ctx.save();
        ctx.globalAlpha = this.rangeAlpha;
        ctx.beginPath();
		ctx.arc(this._x, this._y, this.range, 0, 2 * Math.PI, false);
		//ctx.fillStyle = 'green';
		//ctx.fill();
		ctx.lineWidth = 5;
		ctx.strokeStyle = this.rangeColorOutline;
		ctx.fillStyle = this.rangeColor;
		if(this.outlineCircle){  ctx.stroke()};
		ctx.fill();
		ctx.restore();
	},

	setNewRange: function(range){
		
		var delayTime = 300;
		var time = 1600;
		this.delay(function(){
			this.tween({range: range},time);
			this.rangeColor = mycolors.selectionRange2;
			this.blinkCounter = 0;
			this.outlineCircle = true;
			this.rangeAlpha = 0.12;
			
			
		},delayTime,0);
		
		this.delay(function(){
			this.rangeColor = mycolors.selectionRange;
			this.rangeAlpha = 0.08;
			this.outlineCircle = false;
			// this.range = range;
			this.rangeCircle = new Crafty.circle(this._x, this._y, this.range);
			this.attach(this.rangeCircle);
			
		}, delayTime + time, 0);
		
		
	},



	

});

Crafty.c('Player', {
	init: function() {
		this.requires('2D, Canvas, KeyControls, Moving, Collision, OriginCoordinates, playerSprite, SpriteAnimation, Tween').attr({
			w: 35,
			h: 35,
		});
		this.collision(new Crafty.polygon(10,8,10,34,25,34,25,8));  // old 40x40 pic
		// this.collision(new Crafty.polygon(20,10,20,58,40,58,40,10));
		this.origin('center');
		Crafty.viewport.follow(this);
		//Crafty.viewport.clampToEntities = false;
    	Crafty.viewport.bounds = {min:{x:0, y:0}, max:{x:40 * 80, y: 40 * 80}};
    	this.reel('Walking',750,[[1,0],[2,0],[3,0],[4,0],[5,0],[0,0]]);
    	this.reel('Standing',150,0,0,1);
    	this.reel('Jumping',150,6,0,1);
    	this.reel('Falling',150,7,0,1);
    	this.falling = 0;
    	this.state = "standing";
    	this.dead = false;




	},
	_Player: function(x, y) {
		this.realX = x;
		this.realY = y - this.h;
		this.x = Math.round(this.realX);
		this.y = Math.round(this.realY);
		this.z = params.zLevels['player'],
		this._KeyControls(Crafty.keys.LEFT_ARROW, Crafty.keys.RIGHT_ARROW, Crafty.keys.UP_ARROW, Crafty.keys.DOWN_ARROW);
		this._Moving();
		this.hair = Crafty.e('2D, Canvas, hair, SpriteAnimation').attr({x:this.x, y:this.y, z: params.zLevels["player"]+2});
		this.hair.reel('Walking', 750,0,0,1);//.animate('Walking',-1);
		this.hair.reel('Standing', 750,0,0,1);
		this.hair.reel('Jumping', 750,1,0,1);
		this.hair.reel('Falling', 750,2,0,1);
		this.attach(this.hair);
		this.crown = Crafty.e('2D, Canvas, crown, SpriteAnimation').attr({x:this.x, y:this.y,  z: params.zLevels['player']+3});
		this.crown.attr({visible: false});
		this.crown.reel('Walking', 750,0,0,1);//.animate('Walking',-1);
		this.crown.reel('Standing', 750,0,0,1);
		this.crown.reel('Jumping', 750,1,0,1);
		this.crown.reel('Falling', 750,2,0,1);
		this.attach(this.crown);
		this.scepter = Crafty.e('2D, Canvas, scepter, SpriteAnimation').attr({x:this.x, y:this.y,  z: params.zLevels['player']-1});
		this.scepter.attr({visible: false});
		this.scepter.reel('Walking', 750,0,0,6);//.animate('Walking',-1);
		this.scepter.reel('Standing', 750,0,0,1);
		this.scepter.reel('Jumping', 750,6,0,1);
		this.scepter.reel('Falling', 750,7,0,1);
		this.attach(this.scepter);
		this.amulet = Crafty.e('2D, Canvas, amulet, SpriteAnimation,Tween').attr({x:this.x, y:this.y, z: params.zLevels["player"]+1});
		this.amulet.attr({visible: false});
		this.amulet.origin('center');
		this.amulet.reel('Walking', 750,0,0,1);//.animate('Walking',-1);
		this.amulet.reel('Standing', 750,0,0,1);
		this.amulet.reel('Jumping', 750,1,0,1);
		this.amulet.reel('Falling', 750,2,0,1);
		this.attach(this.amulet);

		// add circle for selectionRange
		this.selectionRange = Crafty.e('SelectionRange')._SelectionRange(this.originX(), this.originY());

		this.attach(this.selectionRange);
		this.selectionRange.range = params.selectionRange;

		if (params.debug) {
			this.crown.visible = this.savedCrown = true;
			this.scepter.visible = this.savedScepter = true;
			this.amulet.visible = this.savedAmulet = true;
			// this.selectionRange.setNewRange(params.selectionRange + params.rangeIncrease);
		} else {
			this.crown.visible = false;
			this.scepter.visible = false;
			this.amulet.visible = false;
			this.savedCrown = false;
			this.savedAmulet = false;
			this.savedScepter = false;		
		}
		return this;
	},

	moveCollisionTest: function(){
		if (!this.dead) {
			if (this.hit("Bullet")) {
				respawnPlayer();
			}
			if (this.hit('Bouncer') || this.hit('Minion')) {
				respawnPlayer();
			}
			if (this.hit('RedScepter') && !this.scepter.visible) {
				this.scepter.visible = true;
				this.attach(Crafty.e("2D,Canvas,Particles, Delay").particles(optionsParticlesScepter).attr({x:player.x, y:player.y, z:player.z-19}).delay(function(){this.destroy()},1400,0));
			}
			if (!this.crown.visible && this.hit('WhiteCrown')) {
				this.attach(Crafty.e("2D,Canvas,Particles, Delay").particles(optionsParticlesCrown).attr({x:player.x, y:player.y, z:player.z-19}).delay(function(){this.destroy()},1400,0));
				this.crown.visible = true;
				this.hair.visible = false;
				// player.selectionRange.tween({range:params.selectionRange + params.rangeIncrease},600);
				this.selectionRange.setNewRange(params.selectionRange + params.rangeIncrease);
			}
			if (this.hit('Amulet') && !this.amulet.visible) {
				this.amulet.visible = true;
				var originalh = this.amulet.h;
				this.amulet.h = 70;
				this.amulet.w = 70;
				this.amulet.tween({h: originalh, w:originalh, rotation: 360 },700);
				this.attach(Crafty.e("2D,Canvas,Particles, Delay").particles(optionsParticlesAmulet).attr({x:player.x, y:player.y, z:player.z-19}).delay(function(){this.destroy()},1400,0));
			}

			var explanations = this.hit('Explanation')
			if (explanations ) {
				epl = explanations[0].obj.board.visible = true;
			} else{
				Crafty('Explanation').each(function(){this.board.visible = false});
			}

			var goals = this.hit('Goal')
			if (goals ) {
				
				if(!this.win){
					this.win = true;
					goal = goals[0].obj;
					goal.board.visible = true;
					goal.board.text(texts.win + "<br>You only died " + deathCount + " times.");
				}
			} 

			var saves = this.hit('Save');
			if (saves) {
				var s = saves[0].obj;
				Crafty("Save").each(function(){
					if (s!== this) {
						this.animate("Off",-1);					
					}
				});
				if (!(s.isPlaying("Flame"))) {
					s.animate("Flame",-1);				
				}
				respawnPos = [s._x, s._y];
				this.savedCrown = this.crown.visible;
				this.savedAmulet = this.amulet.visible;
				this.savedScepter = this.scepter.visible;
			}
			if (this.hit('Goal')) {

				//playBoss();
			}
		}

		var collisions = this.hit(['Platform']);
		if (collisions) {
			return collisions;
		}
		return false;
	},

	walk: function(){
		
		// this.resumeAnimation('Walking',-1);
		// this.scepter.resumeAnimation('Walking',-1);
	 	if(this.state !== "walking"){
 			this.animate('Walking',-1);
			this.scepter.animate('Walking',-1);
			this.crown.animate('Walking',-1);
			this.hair.animate('Walking',-1);
			this.amulet.animate('Walking',-1);
			this.state = "walking";
		}
	},

	stand: function(){
		if(this.state !== "standing"){
			this.animate('Standing',-1);
			this.scepter.animate('Standing',-1);
			this.crown.animate('Standing',-1);
			this.hair.animate('Standing',-1);
			this.amulet.animate('Standing',-1);
			this.state = "standing";
		}
	},

	fall: function(){
		if(this.state !== "falling"){
			this.falling = 0;
			this.state = "falling";
		} else{
			if(this.falling >= 7){
				this.animate('Falling',-1);
				this.scepter.animate('Falling',-1);
				this.crown.animate('Falling',-1);
				this.hair.animate('Falling',-1);
				this.amulet.animate('Falling',-1);
			} else {
				this.falling += 1;
			}

		}
	},

	jump: function(){
		if(this.state !== "jumping"){
			this.animate('Jumping',-1);
			this.scepter.animate('Jumping',-1);
			this.crown.animate('Jumping',-1);
			this.hair.animate('Jumping',-1);
			this.amulet.animate('Jumping',-1);
			this.state = "jumping";
			
		}
	},

});
