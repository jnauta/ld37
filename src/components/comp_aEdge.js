Crafty.c("aEdge", {
	init: function(){
		this.requires("2D, Canvas, Edge, OriginCoordinates, Delay");
		
		
	},

	_aEdge: function(n1, n2) {
		// Body of snake
		this.n1 = n1;
		this.n2 = n2;
		this.snakeBody = Crafty.e("2D, Canvas, Collision, OriginCoordinates, Image,Tween, Platform,").image(imageMap["snakeBody" + this.n1.color.toString()], "repeat");
		this.attach(this.snakeBody);
		this.snakeBody.z = params.zLevels["snakes"];
		this.snakeBody.h = 12;
		this.snakeBody.origin(0, this.snakeBody.h/2);
		this.snakeBody.x = n1.originX();//Math.min(n1.originX(), n2.originX());
		this.snakeBody.y = n1.originY()-this.snakeBody.h/2;//Math.min(n1.originY(), n2.originY());
		var rotation = (180 / Math.PI) * Math.atan2(n2.y - n1.y, n2.x - n1.x);
		this.flipHeadEnd = false;
		if(rotation<-90 || rotation >90){
			this.flipHeadEnd = true;
		}
		this.snakeBody.rotation = rotation; 
		this.snakeHead = Crafty.e('2D,Canvas, snakeHead, Tween').sprite(0,this.n1.color);
		this.attach(this.snakeHead);
		this.snakeHead.attr({h:20,w:20}).origin(10,10).attr({x:n1._x, y:n1._y, rotation: rotation, z: params.zLevels["nodes"]+1});
		this.activate();
		return this;
	},
	activate: function() { // black
		var n1 = this.n1;
		var n2 = this.n2;
		var endWidth =  Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		this.snakeBody.attr({w:2});
		this.snakeBody.tween({w: endWidth}, Math.floor(endWidth/2), "smoothStep");
		// head of snake 
		// fix that head doesnt quite reach letter:
		var headX = 12*(n2.x-n1.x)/(endWidth);
		var headY = 12*(n2.y-n1.y)/(endWidth);
		this.snakeHead.tween({x:n2._x-headX, y: n2._y-headY},Math.floor(endWidth/2));
		this.delay(function(){
			if(this && this.snakeHead){
				this.snakeHead.sprite(1,this.n1.color);
				if(this.flipHeadEnd){
					this.snakeHead.flip("Y");
				}
			}},Math.floor(endWidth/2),0);
		this.active = true;

		this.active = true;
		this.snakeBody.visible = true;
		this.snakeHead.visible = true;
		var n1 = this.n1;
		var n2 = this.n2;
		this.snakeBody.requires('Platform');
	},
	deactivate: function() { // black
		this.active = false;
		var n1 = this.n1;
		var n2 = this.n2;
		console.log('w : ' + this.w);
		//var endWidth = Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		this.snakeBody.tween({w: 2}, this.snakeBody.w, "smoothStep");
		this.snakeHead.tween({x:n1._x, y: n1._y},this.snakeBody.w);
		this.delay(function(){
			if(this.snakeHead){
				this.snakeHead.visible = false;
				this.snakeBody.visible = false;
				this.snakeBody.removeComponent("Platform");
			}},this.snakeBody.w,0);

	}

});

Crafty.c("bEdge", {
	init: function(){
		this.requires("2D, Canvas, Edge, Collision, OriginCoordinates, Image,Tween, Platform");
		this.z = params.zLevels["edges"];
	},

	_bEdge: function(point1, point2, type) {	
		this.image(imageMap["edge"+ type.toString()], "repeat");
		this.h = 10;
		this.origin(0, this.h/2);
		this.x = point1[0];//Math.min(n1.originX(), n2.originX());
		this.y = point1[1] - this.h/2;//Math.min(n1.originY(), n2.originY());
		this.rotation = (180 / Math.PI) * Math.atan2(point2[1] - point1[1], point2[0] - point1[0]);
		this.w =  Math.sqrt((point2[0] - point1[0]) * (point2[0] - point1[0]) + (point2[1] - point1[1]) * (point2[1] - point1[1] ));
		
		return this;
	},

});



Crafty.c("edgeConnector", {

	_edgeConnector: function(startPoint, polyline, type) {			
		var firstPoint = [startPoint[0] + polyline[0].x, startPoint[1] + polyline[0].y];
		var nextPoint;
		Crafty.e('2D,Canvas,Image').image(imageMap["circle" + type.toString()]).attr({x:firstPoint[0]-5,y:firstPoint[1]-5, z:params.zLevels["edges"]+1});
		for(var i = 0; i< polyline.length-1; i++){
			nextPoint = [startPoint[0] + polyline[i+1].x, startPoint[1] + polyline[i+1].y];	
			Crafty.e('2D,Canvas,Image,').image(imageMap["circle"+ type.toString()]).attr({x:nextPoint[0]-5,y:nextPoint[1]-5, z:params.zLevels["edges"]+1});		
			
			Crafty.e('bEdge')._bEdge(firstPoint, nextPoint, type);			
			firstPoint = nextPoint;
		}
		
		return this;
	},

});