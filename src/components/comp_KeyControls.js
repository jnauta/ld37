Crafty.c('KeyControls', {
	init: function() {
		this.goingLeft = false;
		this.goingRight = false;
		this.firstNode = null;
		this.currentEdge = null;
		this.jumping = false;
		this.airborne = true;
		this.jumpFrames = 0;
		this.bind('KeyDown', function(keyEvent) {
			if(!player.dead){
				if(keyEvent.key >= 65 && keyEvent.key < 91){ // pressing letter selects node
					var nodes = Crafty("Node");
					for (var i = nodes.length - 1; i >= 0; i--) {
						var node = Crafty(nodes[i]);
						if (node.keyNumber === keyEvent.key){
							if(!node.selected){
								if(node.selectable) {
									node.select();
									if (node.color === 1) { // red
										if (!!this.redSnake && node !== this.redSnake) {
											this.redSnake.deselect();
										}
										this.redSnake = node;
										toggleTargetLabels(true);
									} else if (node.color === 5 && !!this.redSnake) {// targets
										this.redSnake.fireOn(node.enemy);
										break;
									} else if (!!this.firstNode) { // one node has already been selected, create new edge
										if (this.firstNode.color === 0 && node.color === 0) { // green										
											if (this.currentEdge) {
												this.currentEdge.destroy();											
											}
											this.currentEdge = Crafty.e('aEdge')._aEdge(this.firstNode, node);
											if(!mutesound){Crafty.audio.play('sssnake',1,1);}
											// Crafty.audio.play('sssnake',1,0.5)
											this.firstNode.deselect();
											this.firstNode = null;
											node.deselect();
										} else if (this.firstNode.color === 2 && node.color === 2) { // black
											if (node.snake.active) {
												node.snake.deactivate();
											}
											this.firstNode.deselect();
											this.firstNode = null;
											node.deselect();
										} else if (this.firstNode.color === 0 && node.color === 2 ||
												   this.firstNode.color === 2 && node.color === 0) {
											this.firstNode.deselect();
											this.firstNode = node;
										}
									} else { // no node selected yet, let this one be the first! green or black
										this.firstNode = node;
									}
								}
							} else { // node was already selected
								node.deselect();
								if (node === this.firstNode) {
									this.firstNode = null;
								}
								if (node === this.redSnake) {
									this.redSnake = null;
									toggleTargetLabels(false);
								}
							}
						}
					}
				} else if (keyEvent.key === Crafty.keys.SPACE && this.currentEdge) {
					this.currentEdge.destroy();
				} else{// moving	 		
					if (keyEvent.key === this.up && (!this.airborne || this.jumpSlackFrames > 0)) { // jumpSlackFrames is reset in enterFrame
						this.jumping = true;
						this.jumpFrames = 0;
						// if(!mutesound){Crafty.audio.play('jump0',1,1);}
						if(this.state && this.state !== "jumping"){this.jump();}
					} else if (keyEvent.key === this.down && player.scepter.visible) {
						toggleTargetLabels();
					} else if (keyEvent.key === this.right ) {
						if(!this.airborne){this.walk()};
						this.goingRight = true;
						this.unflip('X');
						this.hair.unflip('X');
						this.crown.unflip('X');
						this.scepter.unflip('X');
						this.amulet.unflip('X');
					} else if (keyEvent.key === this.left ) {
						if(!this.airborne){this.walk()};
						this.goingLeft = true;
						this.flip('X');
						this.hair.flip('X');
						this.crown.flip('X');
						this.scepter.flip('X');
						this.amulet.flip('X');
					}
				}
			}
		});
		this.bind('KeyUp', function(keyEvent) {
			if (keyEvent.key === this.up) {
				this.jumping = false;
			} else if (keyEvent.key === this.right) {
				this.goingRight = false;
			} else if (keyEvent.key === this.down) {

			} else if (keyEvent.key === this.left) {
				this.goingLeft = false;
			}
		});
		this.bind('EnterFrame', function(){
			if (this.jumpSlackFrames > 0) {
				++this.jumpSlackFrames;
				if (this.jumpSlackFrames === params.jumpSlack) {
					this.jumpSlackFrames = 0;
				}
			}
			if (this.jumping) {
				if ((!this.airborne) || (this.jumpSlackFrames > 0)) {
					this.vy = -params.playerJump;
					this.airborne = true;
					this.jumpSlackFrames = 0;
					//this.sprite(1,0);
					this.ground = null;
				} else {
					if (this.jumpFrames < params.jumpFrames) {
						this.vy -= params.jumpSustain;
						++this.jumpFrames;
					} else {
						// console.log('already airborne, jumpframes up');
						//this.jumping = false;
						//this.jumpFrames = 0;
					}
				}
			}
			if (this.goingLeft && this.vx > -params.playerMaxSpeed) {
				this.vx -= params.playerAcceleration;
			}
			if (this.goingRight && this.vx < params.playerMaxSpeed) {
				this.vx += params.playerAcceleration;
			}
			if(this.vy > 0){
			}
			if (this.vy < params.gravityMaxSpeed) {
				this.vy += params.gravityAcceleration;
			}
			if (this.vx > params.playerSlowDown) {
				this.vx -= params.playerSlowDown;
			} else if (this.vx < -params.playerSlowDown) {
				this.vx += params.playerSlowDown;
			} else if(this.vx === 0){

			} else {
				this.vx = 0;
			}
		});
	},
	_KeyControls: function(left, right, up, down) {
		this.left = left;
		this.right = right;
		this.up = up;
		this.down = down;
		return this;
	}

});