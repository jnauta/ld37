Crafty.c('Background', {
	init: function(){
		this.requires('2D, Canvas, tiles, OriginCoordinates');
		//this.color("mycolors.platformcolor");
		this.h = 40;
		this.w = 40;
		this.z = params.zLevels['background'];
		this.origin('center');
	},
	
	_Background: function(x,y){
		this.x = 40*x;
		this.y = 40*y;
		return this;
	},
});