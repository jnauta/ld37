Crafty.c("Node", {
	init: function(){
		this.requires("2D, Canvas, OriginCoordinates, letterA");
		this.attr({x:0,y:0, h:20, w:20});
		this.selected= false;
		this.selectable = true;
		this.origin('center');
		this.z = params.zLevels["nodes"];
	},

	_Node: function(x,y,letter,color) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.letter = letter % 26;
		//this.sprite(randomLetters[this.letter], this.color);
		this.keyNumber = 65 + randomLetters[this.letter];
		this.snakeHead = Crafty.e('2D,Canvas, snakeHead, Tween')
			.attr({x:this.x+14, y: this.y, visible: false, z: params.zLevels["edges"]+1})
			.sprite(0,this.color).origin(-4,10);
		this.bind('EnterFrame',function(){
				if(this.snakeHead.visible  && this.color === 0){
					var dx = player.originX() - this.originX();
					var dy = player.originY() - this.originY();
					this.snakeHead.rotation =  consts.rad2deg * Math.atan2(dy,dx);
				}
			})
		this.attach(this.snakeHead);

		// red snake
		//this.shootStart = typeof shootStart !== 'undefined' ? shootStart : params.shootStart;
		this.shootEnd = params.shootEnd;
		//this.shootInterval = shootInterval ? shootInterval : params.shootInterval;
		//this.rangeSquared = rangeSquared ? rangeSquared : params.shooterRangeSquared;
		this.shootCounter = 0;
		if (this.color === 1) { //red
			this.bind("EnterFrame", function(){
				if (this.target) {

					if (this.shootCounter === 0) {			
						Crafty.e('Fireball')._Fireball(this.originX(), this.originY(), this.target, this);
						var angle = Math.atan2(this.target.originY() - this.originY(), this.target.originX() - this.originX());						
						this.snakeHead.tween({rotation:consts.rad2deg * angle},100);
					}
					++this.shootCounter;
					if (this.shootCounter === this.shootEnd) {
						this.shootCounter = 0;
					}
				} else if (this.shootCounter !== 0) {
					++this.shootCounter;
					if (this.shootCounter === this.shootEnd) {
						this.shootCounter = 0;
					}
				}
			});
		}

		return this;
	},

	selectableFun: function(selectable){
		this.selectable = selectable;
		if (!this.selected) {
			if (this.color === 0
			 || (this.color === 1 || this.color === 5) && player.scepter.visible
			 || this.color === 2 && player.amulet.visible) {
				if(this.selectable){
					this.sprite(randomLetters[this.letter], this.color + 6);
				}else{
					this.sprite(randomLetters[this.letter], this.color);
				}
			} else {
				this.sprite(26, this.color); // letters not yet visible, item must be found.
			}
		}

	},

	select: function(){	
		this.selected = true;
		this.sprite(randomLetters[this.letter], 4); // overruled by something, maybe selectableFun
		if (this.color !== 2) {
			this.snakeHead.attr({visible : true});
		}
	},

	deselect: function(){
		if(this.selected){
			this.selected = false;
			this.target = null;
			this.sprite(randomLetters[this.letter], this.color);
			this.snakeHead.attr({visible : false});	
		}
	},

	fireOn: function(target) {
		this.target = target;
	},

	refreshLetter: function() {
		this.keyNumber = 65 + randomLetters[this.letter];
		if (this.color === 0
		 || (this.color === 1 || this.color === 5) && player.scepter.visible
		 || this.color === 2 && player.amulet.visible) {
			this.sprite(randomLetters[this.letter], this.color);
		} else {
			this.sprite(26, this.color); // letters not yet visible, item must be found.
		}
	}
});