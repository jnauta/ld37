Crafty.c('Platform',{
	init: function(){
		this.requires('2D, Canvas, OriginCoordinates, Collision');
		//this.color("mycolors.platformcolor");
		this.h = this.h ? this.h : 40;
		this.w = this.w ? this.w : 40;
		this.z = params.zLevels['platforms'];
		this.origin('center');
    //this.requires("WiredHitBox");
		this.sticky = false;
	},
	
	_Platform: function(x,y){
		this.requires('tiles').sprite(0,0);
		this.x = 40*x;
		this.y = 40*y;
		return this;
		
	},
	
}); 