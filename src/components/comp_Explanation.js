Crafty.c("Explanation", {
	init: function() {
		this.requires("2D, Canvas, Image, Collision");
		this.image(imageMap["explanation"]);
		this.z =  params.zLevels["outside"]-10;
		this.attr({x:200,y:100, }) ;

		this.collision(-40,-40,80,-40,80,40,-40,40);
		
		this.board = Crafty.e('2D,DOM, Text, Keyboard');
		
		// this.attach(this.board);
		this.board.textFont({size: '20px',  family: fontFamily1});
		this.board.css({'border-radius':' 25px','cursor': 'default' ,'background-image':'url('+ imageMap["stone"] + ')','color':'#544E34','box-shadow':'5px 5px 0px #6E5D3D','border':'1px solid #6E5D3D' ,'padding':'20px'});
		// this.bind('killText',function(){ this.destroy()});
		

		// this.button = Crafty.e('2D, DOM,  Text, Keyboard').attr({x:300,y:430,h:40,w:200,z:1110})
		// .css({'text-align':'center', 'color':mycolors.button1Text,'background-color':mycolors.button1,'cursor':'pointer','border-radius':'5px'})
		// .textFont({size: '32px',  family: 'Impact'})
		// .bind('MouseOver',function(){this.css({	'background-color':mycolors.button1Hover, })})
		// .bind('MouseOut',function(){this.css({	  'background-color':mycolors.button1, })});
		// this.button.nr = 0;
		// this.attach(this.button);
		// this.bind('changeText',function(){ 
		// 	this.text('Tutorial text');
		// 	this.button.text('Let\'s start!');
		// 	});
		// Crafty.pause(1);
	},
	
	_Explanation : function(x,y,w,h,text){
		this.x = x;
		this.y = y;
		this.board.text(text);

		this.board.attr({x: this.x -w/2 +20 , y: this.y -h -80});
		this.board.attr({w: w, h: h, alpha: 1, visible:false});
		// this.button.text(buttonText);
		// this.button.bind('KeyDown',function(){
		// 	if (this.isDown('ENTER') ){
  //           	Crafty.pause(0);
  //           	Crafty.trigger('killText');			
  //           }
		// });
		return this;
	}
	
});
