Crafty.c("Shooter", {
	init: function(){
		this.requires("2D, Canvas, OriginCoordinates, statue, Tween");
		
		//this.image(imageMap["shooter"]);
		this.z = params.zLevels["enemies"];
		this.shootCounter = 0;
		this.active = false;
		this.stunCounter = 0;
	},

	_Shooter: function(x, y, shootStart, shootEnd, shootInterval, shootDir, rangeSquared, counterStart, spread, stunFrames) {
		this.h = 80;
		this.w = 40;
		this.x = x;
		this.y = y;
		this.shootStart = typeof shootStart !== 'undefined' ? shootStart : params.shootStart;
		this.shootEnd = typeof shootEnd !== 'undefined' ? shootEnd : params.shootEnd;
		this.shootInterval = shootInterval ? shootInterval : params.shootInterval;
		this.shootDir = typeof shootDir !== 'undefined' ? consts.deg2rad * shootDir : null;
		this.rangeSquared = rangeSquared ? rangeSquared : params.shooterRangeSquared;
		this.shootCounter = counterStart ? counterStart : 0;
		this.spread = spread ? consts.deg2rad * spread : 0;
		this.stunFrames = typeof stunFrames !== 'undefined' ? stunFrames : params.shooterStunFrames; 

		this.origin(this.w / 2, 11 * this.h / 32);
		// this.w = Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		//this.rotation = rotation;
		this.bind("EnterFrame", function(){
			if (!this.stunCounter) {
				if (this.shootCounter === 0) {
					// check whether player is close
					var dx = player.originX() - this.originX();
					var dy = player.originY() - this.originY();
					if (dx * dx + dy * dy < this.rangeSquared) {
						this.active = true;
					} else {
						this.active = false;
					}
				}
				++this.shootCounter;
				if (this.active && this.shootCounter > this.shootStart && this.shootCounter % this.shootInterval === 0) {
					var angle = this.shootDir;
					if (this.shootDir === null) {
						var bulletDx = player.originX() - this.originX();
						var bulletDy = player.originY() - this.originY();
						angle = Math.atan2(bulletDy, bulletDx);
					}
					if (this.spread !== 0.0) {
						angle += 2.0 * (Math.random() - 0.5) * this.spread;
					}
					Crafty.e('Bullet')._Bullet(this.originX(), this.originY(), Math.cos(angle) * params.vBullet, Math.sin(angle) * params.vBullet);
				}
				if (this.shootCounter === this.shootEnd) {
					this.shootCounter = 0;
				}
			} else {
				--this.stunCounter;
				if (this.stunCounter === 0) {
					this.sprite(0, 0);
				}
			}
		});
		this.targetNode = Crafty.e('Node')._Node(this.originX() + 10, this.originY() + 5 , params.nodeLetters + Math.floor(Math.random() * (26 - params.nodeLetters)), 5); // 5 is targetnode "color"
		this.targetNode.enemy = this;
		this.attach(this.targetNode);
		return this;
	},
	stun: function() {
		this.shootCounter = 0;
		this.sprite(1, 0);
		this.stunCounter = this.stunFrames;//params.shooterStunFrames;
	},
	toggleLabel: function(value) {
		if (typeof value === 'undefined') {
			this.targetNode.visible = this.targetNode.visible ? false : true;
		} else {
			this.targetNode.visible = value;
		}
	}

});

Crafty.c("Bullet", {
	init: function() {
		this.requires("2D, Canvas, OriginCoordinates, Color, BulletMoving, Collision");
		this.color('red');
		//this.image(imageMap["shooter"]);
		this.z = params.zLevels["bullets"];
		this.w = this.h = 5;
		this.origin("center");
		this._BulletMoving();
	},

	_Bullet: function(x, y, vx, vy) {
		this.x = x - this.w / 2;
		this.y = y - this.h / 2;
		this.vx = vx;
		this.vy = vy;
		this.bind("EnterFrame", function() {
			if (this.hit("Platform")) {
				this.destroy();
			};
		});
		return this;
	},

	moveCollisionTest: function(){
		return false;
	},
});