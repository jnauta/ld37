Crafty.c("Minion", {
	init: function(){
		this.requires("2D, Canvas, OriginCoordinates, scorpion, SpriteAnimation, Tween, MinionMoving, Collision");
		//this.image(imageMap["shooter"]);
		this.z = params.zLevels["enemies"];
		this._MinionMoving();
		this.reel('Walking',300,0,0,4).animate('Walking',-1);
		this.hitBoxRight = new Crafty.polygon(2, 5, 15, 5, 38, 15, 38, 32, 2, 32);//(0, 0, 20, 0, 20, 20, 0, 20);//Crafty.polygon(0, 5, 10, 0, 20, 5, 40, 20, 40, 30, 0, 30)
		this.hitBoxLeft = new Crafty.polygon(38, 5, 25, 5, 2, 15, 2, 32, 38, 32);//0, 0, 40, 0, 40, 30, 0, 30);//Crafty.polygon(40, 5, 30, 0, 20, 5, 0, 20, 0, 30, 40, 30)
		this.collision(this.hitBoxRight);
		this.flipDir = "right";
	},

	_Minion: function(x, y, leftBound, rightBound, speed) {
		this.h = this.w = 40;
		this.x = x;
		this.y = y;
		this.leftBound = leftBound;
		this.rightBound = rightBound;
		this.speed = typeof speed !== 'undefined' ? speed : params.minionSpeed;
		this.vx = this.speed;
		this.origin("center");
		// this.w = Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		//this.rotation = rotation;
		this.bind("EnterFrame", function(){
			if (typeof this.leftBound !== 'undefined' && this._x < this.leftBound * tileSize) {
				this.vx = this.speed;
				this.flipLeft();
			} else if (typeof this.rightBound !== 'undefined' && this._x > this.rightBound * tileSize) {
				this.vx = -this.speed;
				this.flipRight();
			}
			if (this.vy < params.gravityMaxSpeed) {
				this.vy += params.gravityAcceleration;
			}
		});
		this.targetNode = Crafty.e('Node')._Node(this.originX() + 10, this.originY() + 5 , params.nodeLetters + Math.floor(Math.random() * (26 - params.nodeLetters)), 5); // 5 is targetnode "color"
		this.targetNode.enemy = this;
		this.attach(this.targetNode);
		return this;
	},

	moveCollisionTest: function(dimension) {
		var platformHits = this.hit('Platform');
		if (platformHits) {
			return platformHits;
		}
		return false;
	},

	flipLeft: function() {
		// first see if new hitbox does not collide with platforms
		if (this.flipDir === "right") {		
			this.collision(this.hitBoxLeft);
			if (this.moveCollisionTest()) {
				this.collision(this.hitBoxRight);
			} else {
				this.flip();
				this.flipDir = "left";
			}
		}
	},

	flipRight: function(){
		if (this.flipDir === "left") {
			this.collision(this.hitBoxRight);
			if (this.moveCollisionTest()) {
				this.collision(this.hitBoxLeft);
			} else {
				this.unflip();
				this.flipDir = "right";
			}
		}
	},
	toggleLabel: function(value) {
		if (typeof value === 'undefined') {
			this.targetNode.visible = this.targetNode.visible ? false : true;
		} else {
			this.targetNode.visible = value;
		}
	}

});