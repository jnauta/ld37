Crafty.c("Moving", {
	init: function(){
		this.requires("2D");
		this.vx = 0.0;
		this.vy = 0.0;
	},

	_Moving: function() {
		this.realX = this._x;
		this.realY = this._y;
		this.bind("EnterFrame", function() {
			this.x = this.realX;
			this.y = this.realY;
			this.prevX = this.x;
			this.prevY = this.y;
			var newGround = false;
			var groundBox = null;
			// change displacement depending on ground slope
			if (this.ground) {
				var radRotation = 0;
				var slopeVx = null;//Math.cos(radRotation) * this.vx;
				var slopeVy = null;//Math.sin(radRotation) * this.vx;

				groundBox = this.ground.mbr();
				if(this._y + this._h > groundBox._y) {
					var radRotation = consts.deg2rad * ((this.ground._rotation + 360) % 180);
					if (radRotation !== 0.5 * Math.PI) {
						if (radRotation > 0.5 * Math.PI) {
							radRotation -= Math.PI;
						}
						slopeVx = Math.cos(radRotation) * this.vx;
						slopeVy = Math.sin(radRotation) * this.vx;
						this.x += slopeVx;//Math.cos(radRotation) * this.vx;
						this.y += slopeVy;//Math.sin(radRotation) * this.vx;
					} else {
						this.x += this.vx;
					}
				} else {
					this.x += this.vx;
				}
				var vxCollisions = this.moveCollisionTest();
				if (vxCollisions) {
					// if the collision takes place with the current ground, we must be at the edge
					if (vxCollisions[0].obj === this.ground) {
						// adjust slopes, we are probably on the far end of an edge
						var saveVx = slopeVx;
						if (slopeVx > 0) {
							slopeVx = slopeVy;
							slopeVy = -saveVx;
						} else if (slopeVx < 0){
							slopeVx = -slopeVy;
							slopeVy = saveVx;
						}
						this.x = this.prevX + slopeVx;
						this.y = this.prevY + slopeVy;
						vxSecondCollisions = this.moveCollisionTest();
						if (vxSecondCollisions) {
							this.vx = 0;
							this.x = this.prevX;
							this.y = this.prevY;
						}
					} else { // collision because of the 'over the top' movement
						//this.vx = 0;
						this.x = this.prevX;
						this.y = this.prevY;
						this.ground = vxCollisions[0].obj;
						newGround = true;
					}
				}
			} else {
				this.x += this.vx;
				if (this.moveCollisionTest()) {
					this.x = this.prevX;
				}
			}
			if (!newGround) {
				this.prevY = this.y;
				this.y += this.vy;
				var vyCollision = this.moveCollisionTest();
				if (vyCollision) {
					if (this.vy > 0.0) {
						if (vyCollision.constructor === Array) {
							this.ground = vyCollision[0].obj;
							newGround = true;
						}
						this.airborne = false;
						this.jumpFrames = 0;
						if(this.state){
						if(this.state === "falling"){
							if(this.vx!==0){
								this.walk()
								}else{
								this.stand()
								}
							}
						}

					}
					this.vy = 0;
					this.y = this.prevY;
				} else {
					// if (!this.airborne) {
					// 	this.airborne = true;
					// 	this.jumpSlackFrames = 1;
					// }
				}
			}
			if (groundBox && (this._x >= groundBox._x + groundBox._w || this._x + this._w <= groundBox._x)) {
				// totally besides the current ground, we must be falling
				this.jumpSlackFrames = 1;
				this.ground = null;
				this.airborne = true;
			}
			if(this.state){
				if(this.vx === 0 && this.state === "walking"){					
					this.stand();
				}else if(this.vx !== 0 && this.state === "standing" ){
					this.walk();
				}

				if(this.vy >0 && this.airborne){
					this.fall()
					
				}
			}
			this.realX = this.x;
			this.realY = this.y;
			this.x = Math.round(this._x);
			this.y = Math.round(this._y);
		});
		return this;
	}
});

Crafty.c("BouncerMoving", {
	init: function(){
		this.requires("2D");
		this.vx = 0.0;
		this.vy = 0.0;
	},

	_BouncerMoving: function() {
		this.bind("EnterFrame", function() {
			var dx = this._x - player._x;
			var dy = this._y - player._y;
			if (dx * dx + dy * dy < params.playerClose) {
				// initialize Dxs to fly home if far away
				var targetDx, targetDy;
				var followTarget = false;
				if (this.homing) {
					followTarget = true;
					targetDx = player.originX() - this.originX();
					targetDy = player.originY() - this.originY();
				}
				if (typeof this.homeDistance !== 'undefined') {
					var dx = this.startX - this._x;
					var dy = this.startY - this._y;
					if (this.homeDistance && dx * dx + dy * dy > this.homeDistance * this.homeDistance) {
						followTarget = true;
						targetDx = dx;
						targetDy = dy;
					}
				}
				if (followTarget) { // either chasing player or going home
					var targetAngle = consts.rad2deg * Math.atan2(targetDy, targetDx);
					this.angle = ((this.angle + 720) % 360);
					if (this.angle > 180) {
						this.angle -= 360;
					}
					if ((this.angle > targetAngle && this.angle - targetAngle < 180)
					 || (targetAngle > this.angle && targetAngle - this.angle > 180)) {
						this.angle -= this.turnAngle;
					} else {
						this.angle += this.turnAngle;
					}
				} else { // fly in circles or bounce around
					this.angle += this.turnAngle;
				}

				this.vx = Math.cos(consts.deg2rad * this.angle) * this.speed;
				this.vy = Math.sin(consts.deg2rad * this.angle) * this.speed;
				this.prevX = this._x;
				this.prevY = this._y;
				this.x += this.vx;
				this.y += this.vy;
				if (this.moveCollisionTest()) {
					this.x = this.prevX;
					this.y = this.prevY;
				}
			}
		});
		return this;
	}
});

Crafty.c("MinionMoving", {
	init: function(){
		this.requires("2D");
		this.vx = 0.0;
		this.vy = 0.0;
		this.newGroundTested = false;
	},

	_MinionMoving: function() {
		this.bind("EnterFrame", function() {
			var dx = this._x - player._x;
			var dy = this._y - player._y;
			if (dx * dx + dy * dy < params.playerClose) {
				if (this.vx >= 0) {
					this.flipRight();
				} else {
					this.flipLeft();
				}

				this.prevX = this.x;
				this.prevY = this.y;
				var newGround = false;
				var groundBox = null;
				// change displacement depending on ground slope
				if (this.ground) {
					var radRotation = 0;
					var slopeVx = null;//Math.cos(radRotation) * this.vx;
					var slopeVy = null;//Math.sin(radRotation) * this.vx;

					groundBox = this.ground.mbr();
					if(this._y + this._h > groundBox._y) {
						var radRotation = consts.deg2rad * ((this.ground._rotation + 360) % 180);
						if (radRotation !== 0.5 * Math.PI) {
							if (radRotation > 0.5 * Math.PI) {
								radRotation -= Math.PI;
							}
							slopeVx = Math.cos(radRotation) * this.vx;
							slopeVy = Math.sin(radRotation) * this.vx;
							this.x += slopeVx;//Math.cos(radRotation) * this.vx;
							this.y += slopeVy;//Math.sin(radRotation) * this.vx;
						} else {
							this.x += this.vx;
						}
					} else {
						this.x += this.vx;
					}
					var vxCollisions = this.moveCollisionTest("x");
					// if collisions occurred, try to resolve them with different movement
					if (vxCollisions) {
						// if the collision takes place with the current ground, we must be at the edge
						if (vxCollisions[0].obj === this.ground) {
							this.newGroundTested = true;
							// adjust slopes, we are probably on the far end of an edge
							var saveVx = slopeVx;
							if (slopeVx > 0) {
								slopeVx = slopeVy;
								slopeVy = -saveVx;
							} else if (slopeVx < 0){
								slopeVx = -slopeVy;
								slopeVy = saveVx;
							}
							this.x = this.prevX + slopeVx;
							this.y = this.prevY + slopeVy;
							vxSecondCollisions = this.moveCollisionTest("x");
							if (vxSecondCollisions) {
								this.vx = 0;
								this.x = this.prevX;
								this.y = this.prevY;
							}
						} else { // we collided with a different piece of ground
							//this.vx = 0;
							this.x = this.prevX;
							this.y = this.prevY;
							this.ground = vxCollisions[0].obj;
							newGround = true;
							if (this.newGroundTested) {
								this.newGroundTested = false;
								var slopeRotation  = (this.ground._rotation + 360) % 180;
								if (slopeRotation > 90) {
									slopeRotation -= 180;
								}
								if (slopeRotation === 90.0 || (slopeRotation >= 0 && this.vx > 0) || (slopeRotation <= 0 && this.vx < 0)) {
									if (this.vx > 0) {
										this.vx = -this.speed;
										this.flipLeft();
									} else {
										this.vx = this.speed;
										this.flipRight();
									}
								}
							} else {
								this.newGroundTested = true;
							}
						}
					} else {
						this.newGroundTested = false;
					}
				} else {
					this.newGroundTested = false;
					this.x += this.vx;
					if (this.moveCollisionTest("x")) {
						this.x = this.prevX;
					}
				}
				if (!newGround) {
					this.prevY = this.y;
					this.y += this.vy;
					var vyCollision = this.moveCollisionTest("y");
					if (vyCollision) {
						if (this.vy > 0.0) {
							if (vyCollision.constructor === Array) {
								this.ground = vyCollision[0].obj;
								newGround = true;
							}
							this.airborne = false;
						}
						this.vy = 0;
						this.y = this.prevY;
					}
				}
				if (groundBox && (this._x >= groundBox._x + groundBox._w || this._x + this._w <= groundBox._x)) {
					this.ground = null;
					this.airborne = true;
				}
			}
		});
		return this;
	}
});

Crafty.c("BulletMoving", {
	init: function(){
		this.requires("2D");
		this.vx = 0.0;
		this.vy = 0.0;
	},

	_BulletMoving: function() {
		this.bind("EnterFrame", function() {
			this.x += this.vx;
			this.y += this.vy;
		});
		return this;
	}
});