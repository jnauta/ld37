Crafty.c("Bouncer", {
	init: function(){
		this.requires("2D, Canvas, OriginCoordinates, bat, SpriteAnimation, Tween, BouncerMoving, Collision");
		
		//this.image(imageMap["shooter"]);
		this.z = params.zLevels["enemies"];
		this._BouncerMoving();
		this.reel('Flying',300,0,0,2).animate('Flying',-1);
		this.collision(new Crafty.polygon(5,10,0,20,20,28,40,20,35,10));
	},

	_Bouncer: function(x, y, startAngle, turnAngle, homing, homeDistance, speed) {
		this.h = this.w =  40;
		this.startX = x;
		this.startY = y;
		this.x = x;
		this.y = y;
		this.angle = typeof startAngle !== 'undefined' ? startAngle : 0;
		this.turnAngle = typeof turnAngle !== 'undefined' ? turnAngle : params.bouncerTurn;
		this.homing = typeof homing !== 'undefined' ? homing : false;
		this.homeDistance = typeof homeDistance !== 'undefined' ? homeDistance : null;
		this.speed = typeof speed !== 'undefined' ? speed : params.vBouncer;
		this.origin("center");
		// this.w = Math.sqrt((n2.x - n1.x) * (n2.x - n1.x) + (n2.y - n1.y) * (n2.y - n1.y));
		//this.rotation = rotation;
		this.targetNode = Crafty.e('Node')._Node(this.originX() + 10, this.originY() + 5 , params.nodeLetters + Math.floor(Math.random() * (26 - params.nodeLetters)), 5); // 5 is targetnode "color"
		this.targetNode.enemy = this;
		this.attach(this.targetNode);
		return this;
	},

	moveCollisionTest: function() {
		var platformHits = this.hit('Platform');
		if (platformHits) {
			var normal = platformHits[0].normal;
			var normalAngle = consts.rad2deg * Math.atan2(normal.y, normal.x);
						//Crafty.e('2D, Canvas, Color').attr({x: this._x, y: this._y, w: 30, h: 5, rotation: normalAngle}).color('black');
			this.angle = ((this.angle + 720) % 360);
			if (this.angle > 180) {
				this.angle -= 360;
			}
			if (normalAngle > this.angle && normalAngle - this.angle < 180) {
				this.angle = 2 * (normalAngle - 90) - this.angle;
				this.angle = ((this.angle + 720) % 360);
				if (this.angle > 180) {
					this.angle -= 360;
				}
			} else {
				this.angle = 2 * (normalAngle + 90) - this.angle;
				this.angle = ((this.angle + 720) % 360);
				if (this.angle > 180) {
					this.angle -= 360;
				}
			}
			return platformHits;
		}
		return false;
	},
	toggleLabel: function(value) {
		if (typeof value === 'undefined') {
			this.targetNode.visible = this.targetNode.visible ? false : true;
		} else {
			this.targetNode.visible = value;
		}
	}
});