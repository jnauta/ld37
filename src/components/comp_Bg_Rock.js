Crafty.c('Bg_Rock', {
	init: function(){
		this.requires('2D, Canvas, DebugCanvas, WiredHitBox');
		this.z = params.zLevels["outside"] ;
	},

	_Bg_Rock: function(x, y, polyline, type) {
		[this.x,this.y,this.w,this.h] = polylineToXYWH(x,y, polyline);
		
		
		this.bind("Draw", function(e){
			firstPoint = [x + polyline[0].x, y + polyline[0].y];
			var ctx = e.ctx;
			var image = new Image();                                          
        	// image.src = imageMap["textureStonePyramid"];//"https://s-media-cache-ak0.pinimg.com/originals/ba/62/45/ba624504f86336a81fb06c691624d4d4.jpg" ;
        	
        	if(type===0){
        		image.src = imageMap["outside2"];
        	}        		else{
        		image.src = imageMap["filler"];

        		}
			var pat = ctx.createPattern(image, 'repeat');
	        // ctx.lineWidth = 4;
	        // ctx.strokeStyle = mycolors.outside;
	        // ctx.fillStyle = mycolors.outside;
	        ctx.fillStyle=pat;
	        ctx.beginPath();
	        ctx.moveTo(firstPoint[0], firstPoint[1]);
			for(var i = 0; i< polyline.length-1; i++){
				nextPoint = [x + polyline[i+1].x, y + polyline[i+1].y];					
		        ctx.lineTo(nextPoint[0], nextPoint[1]);
		        // ctx.stroke();
			}
	        ctx.fill();
		});
		this.ready = true;
	}
});