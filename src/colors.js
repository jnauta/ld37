
mycolors = {
   
    background : '#673D17', //'#C7B66D', //#D3BDDB',

    outside : '#241304',

    selectionRange: '#E8E856',
    selectionRange2: '#90C3D4',
    selectionRangeOutline: '#33FFff',
    

    title: '#6E3E08',
    titleshadow: '#ffff00ff',    
    buttontext: '#ffffff',
    buttontextshadow: '#003300',
    buttontexthover: '#ffcccc',
    buttontexthovershadow: '#FFFFFF',
    clockTextColor: '#380D02',
    clockTicking : '#C92E04',    
    clockBg: '#DEDBCD',

    stoneExample: '#878682',
    stoneExampleBorder: '#C7C6C3',

    stone: '#86847c', 

    popupBg: '#C4AE56',
    popupText: '#302A12',
    popupBorder: '#302A12',

    popupButtonBg: '#f4AE56',
    popupButtonText: '#302A12',
    popupButtonBorder: '#302A12',

    oldbasket: '#e4ca58',

}