
params = {
    debug: false,
    gravityMaxSpeed: 13.0,
    gravityAcceleration: 0.25,
    playerSlowDown: 0.4,
    playerMaxSpeed: 5.0,
    playerAcceleration: 0.7,
    playerJump: 5.0,
    jumpSlack: 10,
    jumpFrames: 15,
    jumpSustain: 0.3,
    selectionRange: 200,
    rangeIncrease: 50,

    shooterRangeSquared: 250000,
    shootStart: 40,
    shootEnd: 60,
    shootInterval: 4,
    vBullet: 8,
    shooterStunFrames: 250,
    vBouncer: 2,
    bouncerTurn: 2.0,
    minionSpeed: 2.0,
    playerClose: 640000,

    zLevels : {
        background: 1000,
        outside: 1100,
        platforms: 1200,
        things: 1500,
        enemies: 1600,
        edges: 1800,
        snakes: 2000,
        nodes: 2200,
        bullets: 2250,
        player: 3000,
    },
    nodeLetters: 13,
};

consts = {
    deg2rad: Math.PI / 180,
    rad2deg: 180 / Math.PI, 
};


texts = {
    title : "TRIAL OF SNAKES",
    welcome: "Welcome Princess! <br><br> As a test of your wisdom and skills, you have to get to the top of the pyramid and find the royal tomb.<br> Good luck!",
    saving: "If you perish, you will return to the last time that you lit a torch.",
    snakes: "The snakes of the pyramid will help you, press their letters (perhaps a friend can help?) <br>if they are close to you to call upon them.<br> \
    Only one snake will aid you at a time. The spacebar lets them disappear.",
    crown: "The White Crown of Upper Egypt increases your influence over the snakes.",
    amulet: "The wearer of Sekhmet's Black Amulet may banish the snakes that guard the forbidden passages of the pyramid.",
    scepter: "The Red Scepter lets you command Fire Snakes. You may use it to deal with the scorpions below.\
            <br>Press down-arrow to toggle the letters of your foes.",
    win: "Congratulations, you have reached the top of the pyramid. Your ancestors are so proud of you!",

};


optionsParticlesFire = {
  maxParticles: 150,
  size: 18,
  sizeRandom: 4,
  speed: 1,
  speedRandom: 1.2,
  // Lifespan in frames
  lifeSpan: 29,
  lifeSpanRandom: 7,
  // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
  angle: 65,
  angleRandom: 34,
  startColour: [255, 131, 0, 1],
  startColourRandom: [48, 50, 45, 0],
  endColour: [245, 35, 0, 0],
  endColourRandom: [60, 60, 60, 0],
  // Only applies when fastMode is off, specifies how sharp the gradients are drawn
  sharpness: 20,
  sharpnessRandom: 10,
  // Random spread from origin
  spread: 10,
  // How many frames should this last
  duration: -1,
  // Will draw squares instead of circle gradients
  fastMode: false,
  gravity: { x: 0, y: 0.1 },
  // sensible values are 0-3
  jitter: 0,
  // Offset for the origin of the particles
  originOffset: {x: 0, y: 0}
};

optionsParticlesCrown = {
  maxParticles: 250,
  size: 7,
  sizeRandom: 4,
  speed: 5,
  speedRandom: 2.2,
  // Lifespan in frames
  lifeSpan: 60,
  lifeSpanRandom: 20,
  // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
  angle: 0,
  angleRandom: 360,
  startColour: [0, 255, 251, 1],
  startColourRandom: [20, 10, 45, 0],
  endColour: [199, 255, 254, 0],
  endColourRandom: [30, 30, 30, 0],
  // Only applies when fastMode is off, specifies how sharp the gradients are drawn
  sharpness: 70,
  sharpnessRandom: 10,
  // Random spread from origin
  spread: 20,
  // How many frames should this last
  duration: 10,
  // Will draw squares instead of circle gradients
  fastMode: false,
  gravity: { x: 0, y: 0.03 },
  // sensible values are 0-3
  jitter: 0,
  // Offset for the origin of the particles
  originOffset: {x: 0, y: 0}
};

optionsParticlesScepter = {
  maxParticles: 250,
  size: 7,
  sizeRandom: 4,
  speed: 5,
  speedRandom: 2.2,
  // Lifespan in frames
  lifeSpan: 60,
  lifeSpanRandom: 20,
  // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
  angle: 0,
  angleRandom: 360,
  startColour: [255, 20, 20, 1],
   startColourRandom :  [50, 50, 50, 0],
  endColour : [240, 150,150, 0],  
  endColourRandom : [30, 30, 30, 0],
  startColourRandom: [20, 10, 45, 0],
  endColour: [199, 255, 254, 0],
  endColourRandom: [30, 30, 30, 0],
  // Only applies when fastMode is off, specifies how sharp the gradients are drawn
  sharpness: 70,
  sharpnessRandom: 10,
  // Random spread from origin
  spread: 20,
  // How many frames should this last
  duration: 10,
  // Will draw squares instead of circle gradients
  fastMode: false,
  gravity: { x: 0, y: 0.03 },
  // sensible values are 0-3
  jitter: 0,
  // Offset for the origin of the particles
  originOffset: {x: 0, y: 0}
};




optionsParticlesAmulet = {
  maxParticles: 250,
  size: 7,
  sizeRandom: 4,
  speed: 5,
  speedRandom: 2.2,
  // Lifespan in frames
  lifeSpan: 60,
  lifeSpanRandom: 20,
  // Angle is calculated clockwise: 12pm is 0deg, 3pm is 90deg etc.
  angle: 0,
  angleRandom: 360,
  startColour: [255, 255, 255, 1],
  startColourRandom :  [50, 50, 50, 0],
  endColour : [240, 240, 130, 0],
  endColourRandom : [30, 30, 30, 0],
  startColourRandom: [20, 10, 45, 0],
  endColour: [199, 255, 254, 0],
  endColourRandom: [30, 30, 30, 0],
  // Only applies when fastMode is off, specifies how sharp the gradients are drawn
  sharpness: 70,
  sharpnessRandom: 10,
  // Random spread from origin
  spread: 20,
  // How many frames should this last
  duration: 10,
  // Will draw squares instead of circle gradients
  fastMode: false,
  gravity: { x: 0, y: 0.03 },
  // sensible values are 0-3
  jitter: 0,
  // Offset for the origin of the particles
  originOffset: {x: 0, y: 0}
};



