var overlay;
var bgMusic = null;
deathCount = 0;
// LOADING SCENE
Crafty.scene('Main', function() {
	
	Crafty.background(mycolors.background);
	Crafty.timer.FPS(80);
	//Crafty.viewport.bounds = {min:{x:0, y:0}, max:{x:Game.width(), y: Game.height()}};
	if (!bgMusic) {
		//bgMusic = Crafty.audio.play('bgMusic',-1,0.3);
		if(mutemusic && bgMusic.source){
			bgMusic.pause();
		}
	}

	Crafty.timer.FPS(80);

	bossMode = false;
	goldPairs = [];
	goldHoles = [];

	spawnShooters = function() {
		for (var j = 0; j < shooters.length; j++) {
			var obj = shooters[j];
			var props = {};
			if (obj.properties) {
				props = obj.properties;
			}
			if (bossMode) {
				if (props && props.bossMode === true) {
					// spawning in boss mode
					var shooter = Crafty.e('Shooter')
						._Shooter(obj.x, obj.y, props.shootStart, props.shootEnd, props.shootInterval, props.shootDir, props.rangeSquared, props.counterStart, props.spread, props.stunFrames);
				}
			} else { // not in bossMode
				if (!props || !props.bossMode) {
					var shooter = Crafty.e('Shooter')
						._Shooter(obj.x, obj.y, props.shootStart, props.shootEnd, props.shootInterval, props.shootDir, props.rangeSquared, props.counterStart, props.spread, props.stunFrames);
				}
			}
		}
	};

	spawnBouncers = function() {
		for (var j = 0; j < bouncers.length; j++) {
			var obj = bouncers[j];
			var props = {};
			if (obj.properties) {
				props = obj.properties;
			}
			if (bossMode) {
				if (props && props.bossMode === true) {
					// spawning in boss mode
					var bouncer = Crafty.e('Bouncer')._Bouncer(obj.x, obj.y, props.startAngle, props.turnAngle, props.homing, props.maxHomeDistance, props.speed);

				}
			} else { // not in bossMode
				if (!props || !props.bossMode) {
					var bouncer = Crafty.e('Bouncer')._Bouncer(obj.x, obj.y, props.startAngle, props.turnAngle, props.homing, props.maxHomeDistance, props.speed);
				}
			}


			//bouncer.turnAngle = 1.6;
		}
	};

	spawnMinions = function() {
		for (var j = 0; j < minions.length; j++) {
			var obj = minions[j];
			var props = {};
			if (obj.properties) {
				props = obj.properties;
			}
			if (bossMode) {
				if (props && props.bossMode === true) {
					// spawning in boss mode
					var minion = Crafty.e('Minion')
						._Minion(obj.x, obj.y, props.left, props.right, props.speed);

				}
			} else { // not in bossMode
				if (!props || !props.bossMode) {
					var minion = Crafty.e('Minion')
					._Minion(obj.x, obj.y, props.left, props.right, props.speed);
				}
			}
		}
	},

	toggleTargetLabels = function(value) {
		var enemies = ['Minion', 'Bouncer', 'Shooter'];
		for (var i = enemies.length - 1; i >= 0; i--) {
			var enms = Crafty(enemies[i]);
			for (var j = enms.length - 1; j >= 0; j--) {
				Crafty(enms[j]).toggleLabel(value);
			}
		}
	},

	shuffle = function(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;

	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }
	  return array;
	};

	buildLevel = function(){
		// iets van reset
		Crafty('2D').each(function() {
			this.destroy();
		});

		randomLetters = [];
		for (var i = 0; i < 26; ++i) {
			randomLetters.push(i);
		}
		randomLetters = shuffle(randomLetters);
		
		level = TileMaps["testlevel4"];
		var tileSets = level.tilesets;
		var xTiles, yTiles;
		for (var i = tileSets.length - 1; i >= 0; i--) {
			var t = tileSets[i];
			if (t.name === "tiles") {
				// these are the real tiles; solid stuff, background etc.
				tileSize = t.tilewidth;
				xTiles = t.imagewidth / t.tilewidth;
				yTiles = t.imageheight / t.tileheight;
			}
		};


		solidLayer = null;
		bgLayer = null;
		nodes = new Array();
		edges = new Array();

		for (var i = level.layers.length - 1; i >= 0; i--) {
			var layer = level.layers[i];
			if (layer.name === "solid") {
				solidLayer = layer;
			} else if (layer.name === "background") {
				bgLayer = layer;
			} else if (layer.name === "objects") {

				// have to subtract tileSize, bug in Tiled places tiles and objects differently
				// var blackList = ["domain", "temple", "tower", "shepherd", "player"];

				// layer.objects.map(function(x) {
				// 	if (blackList.indexOf(x.type) === -1) {
				// 		x.y -= tileSize;
				// 	}
				// });		
				playerObj = layer.objects.filter(function(x) {
					return x.type === "player";
				});
				shooters = layer.objects.filter(function(x) {
					return x.type === "shooter";
				});
				bouncers = layer.objects.filter(function(x) {
					return x.type === "bouncer";
				});
				minions = layer.objects.filter(function(x) {
					return x.type === "minion";
				});
				saves = layer.objects.filter(function(x) {
					return x.type === "save";
				});
				goals  = layer.objects.filter(function(x) {
					return x.type === "goal";
				});
				helps = layer.objects.filter(function(x) {
					return x.type === "help";
				});


				
				// All objects in their own containers

				// player = Crafty.e('Snake')._Snake(playerObj[0].x, playerObj[0].y, [2, 3, 3, 0, 3, 2, 2, 2, 1, 2, 1]);

				spawnShooters();
				spawnBouncers();
				spawnMinions();
				toggleTargetLabels(false);
				for (var j = 0; j < saves.length; j++) {
					var obj = saves[j];
					Crafty.e("Save, 2D, Canvas, saveTorch, SpriteAnimation").attr({x: obj.x, y: obj.y, z: params.zLevels["things"]+2}).reel("Flame",900,1,0,4).reel("Off",200, 0,0,1);
				}

				for (var j = 0; j < goals.length; j++) {
					var obj = goals[j];
					var w = 330;
					var h = 120;
					if(obj.name === "white_crown"){
						Crafty.e("WhiteCrown, 2D, Canvas, Image").image(imageMap["bigcrown"]).attr({x:obj.x,y:obj.y,z: params.zLevels["things"]});
					} else if(obj.name === "red_scepter"){
						Crafty.e("RedScepter, 2D, Canvas,Image, Collision").image(imageMap["redScepter"]).attr({x:obj.x,y:obj.y, z: params.zLevels["things"]}).collision([30,0,50,0,50,80,30,80]);
					} else if(obj.name === "black_amulet"){
						Crafty.e("Amulet, 2D, Canvas,Image").image(imageMap["blackAmulet"]).attr({x:obj.x,y:obj.y, z: params.zLevels["things"]});
					} else if(obj.name === "goal"){
						goal = Crafty.e("Goal, 2D, Canvas,Image, Collision").image(imageMap["goal"]).attr({x:obj.x,y:obj.y, z: params.zLevels["things"]})
						goal.board = Crafty.e('2D,DOM, Text, Keyboard');		
						goal.board.textFont({size: '20px',  family: fontFamily1});
						goal.board.css({'border-radius':' 25px','cursor': 'default' ,'background-image':'url('+ imageMap["stone"] + ')','color':'#544E34','box-shadow':'5px 5px 0px #6E5D3D','border':'1px solid #6E5D3D' ,'padding':'20px'});
						goal.board.attr({x: obj.x -w/2 +20 , y: obj.y -h -60});
						goal.board.attr({w: w, h: h, alpha: 1, visible:false});



					}
				}

				for (var j = 0; j < helps.length; j++) {
					var obj = helps[j];
					var w = 300;
					var h = 200;
					if (obj.properties) {
						if (obj.properties.h) {
							h = obj.properties.h;
						}
					}
					var help = Crafty.e("Explanation")._Explanation(obj.x,obj.y,w,h, texts[obj.name]);
					if (obj.properties && obj.properties.rotation) {
						help.rotation = obj.properties.rotation;
					} else {
						help.rotation = 0;
					}
					//Crafty.e("Explanation, 2D, Canvas,Image").image(imageMap["goal"]).attr({x:obj.x,y:obj.y, z: params.zLevels["things"]});
				}
			} else if (layer.name === "edges") {
				edges = layer.objects.filter(function(x) {
					return x.type === "edge";
				});
				for (var j = 0; j < edges.length; j++) {
					var obj = edges[j];
					// var line = obj.polyline
					// Crafty.e('bEdge')._bEdge(line[0], line[1]);	
					if(obj.name === "edge"){
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
					}else if(obj.name === "outer_edge"){
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,1);
					}

					if(obj.name === "filler" ){
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
						Crafty.e('Bg_Rock')._Bg_Rock(obj.x, obj.y, obj.polyline, 1);						
					} else if(obj.name === "red_scepter_altar" ){
						var [x,y,w,h] = polylineToXYWH(obj.x, obj.y, obj.polyline);
						Crafty.e('2D,Canvas,Image').image(imageMap["altarRed"]).attr({x:x, y:y, z: params.zLevels["things"]});
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
					}else if(obj.name === "white_crown_altar" ){
						var [x,y,w,h] = polylineToXYWH(obj.x, obj.y, obj.polyline);
						Crafty.e('2D,Canvas,Image').image(imageMap["altarCrown"]).attr({x:x, y:y, z: params.zLevels["things"]});
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
					}else if(obj.name === "amulet_altar" ){
						var [x,y,w,h] = polylineToXYWH(obj.x, obj.y, obj.polyline);
						Crafty.e('2D,Canvas,Image').image(imageMap["altarBlack"]).attr({x:x, y:y, z: params.zLevels["things"]});
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
					}else if(obj.name === "gold_scepter_altar" ){
						var [x,y,w,h] = polylineToXYWH(obj.x, obj.y, obj.polyline);
						Crafty.e('2D,Canvas,Image').image(imageMap["altarGold"]).attr({x:x, y:y, z: params.zLevels["things"]});
						Crafty.e('edgeConnector')._edgeConnector( [obj.x,obj.y], obj.polyline,0);
					}		


				}
			} else if (layer.name === "nodes") {
				nodes = layer.objects.filter(function(x) {
					return x.type === "node";
				});
				var blackPairs = [];
				for (var j = 0; j < nodes.length; j++) {
					var obj = nodes[j];
					var color = 0;
					switch(obj.name){
						case "greenNode":
							color = 0;
							break;
						case "redNode":	
							color = 1;
							break;
						case "blackNode":
							color = 2;
							break;
						case "goldNode":
							color = 3;
							break;
						case "targetNode":
							color = 5;
							break;
						default:
							console.log("switch ended nowhere");
					}
					var letter = null;
					if (obj.properties) {
						letter = obj.properties.letterIdx;
					}
					// keep gold nodes separate, part of endgame
					if (color === 3) {
						// nothing for now, process again in boss mode
					} else {
						var node = Crafty.e('Node')
							._Node(obj.x + 10, obj.y +10 , letter, color); // 0 is green, 1 is red, 2 is balck, 3 is gold, 4 is 'selected', 5 is target
						if (obj.name === "blackNode") {
							var first = blackPairs[obj.properties.blackSnake];
							if(!!first && first.constructor == Array) {
								first.push(node);
							} else {
								blackPairs[obj.properties.blackSnake] = [node];
							}
						}
					}
				}
				for (var j = blackPairs.length - 1; j >= 0; j--) {
					var blackPair = blackPairs[j];
					var blackSnake = Crafty.e('aEdge')._aEdge(blackPair[0], blackPair[1]);
					blackPair[0].snake = blackSnake;
					blackPair[1].snake = blackSnake;
				}

			} else if (layer.name === "bg_objects") {
				rocks = layer.objects.filter(function(x) {
					return x.type === "rock";
				});				
				for (var j = 0; j < rocks.length; j++) {
					var rock = rocks[j];
					Crafty.e('Bg_Rock')
						._Bg_Rock(rock.x, rock.y, rock.polyline, 0);						
				}
			}
		}

		for (var row = 0; row < level.height; ++row) {
			for (var col = 0; col < level.width; ++col) {
				// create tile for platform layer
				tileIdx = solidLayer.data[level.width * row + col];
				if (tileIdx != 0) {
					Crafty.e('2D, Canvas, Platform')._Platform(col, row).attr({
						z: params.zLevels['edges'],
						tileIdx: tileIdx
					}).sprite((tileIdx - 1) % xTiles, Math.floor((tileIdx - 1) / xTiles));
				}
				tileIdx = bgLayer.data[level.width * row + col];
				if (tileIdx != 0) {
					//Crafty.e('2D, Canvas, Background')._Background(col, row).sprite((tileIdx - 1) % xTiles, Math.floor((tileIdx - 1) / xTiles));
				}
				
			};
		};
		
		if (params.debug) {
			player = Crafty.e('Player')._Player(1820, 1350);
		//CHEAT:
		// black amulet: 600, 2400
		// corridor: 1220, 1200
		// red scepter: 2380, 1700
		// batcircle: 1820, 1350
		// white crown: 740, 1900
		// boss: 1550, 550);
		// after batcircle: 1920, 1000
		} else {
			player = Crafty.e('Player')._Player(playerObj[0].x, playerObj[0].y);
		}
		//Crafty('Node').each(function() {this.refreshLetter();});
		respawnPos = [player._x, player._y];

		// Crafty.viewport.follow(player);

		//explanationStone = Crafty.e("Explanation")._Explanation(380,3040,300,200, texts.introtext); // x, y, w, h, text

	}
	respawnPlayer = function() {
		player.dead = true;
		if(!mutesound){Crafty.audio.play('dead',1,1);}
		player.goingLeft = false;
		player.goingRight = false;
		player.tween({rotation:720, h:0, w:0},600);

		
		deathCount += 1 ;
		Crafty.e('Delay').delay(function(){
			
			Crafty('Bullet').each(function() {this.destroy()});
			Crafty('Shooter').each(function() {this.destroy()});
			Crafty('Minion').each(function() {this.destroy()});
			Crafty('Bouncer').each(function() {this.destroy()});
			Crafty('aEdge').each(function() {
				if (this.n1.color === 2) { // black
					this.activate();
				}
			});
			// assign new letters to all Nodes
			randomLetters = shuffle(randomLetters);
			Crafty('Node').each(function() {
				this.refreshLetter();
			});

			player.dead = false;			
			player.attr({x : respawnPos[0], y : respawnPos[1], vx: 0, vy :0, rotation: 0, airborne : true, h: 35, w:35});
			player.realX = player.x;
			player.realY = player.y;
			
			player.crown.visible = player.savedCrown;
			if(player.crown.visible){
				player.hair.visible = false
			}else{ player.hair.visible = true}
			player.scepter.visible = player.savedScepter;
			player.amulet.visible = player.savedAmulet;
			if (!!player.redSnake) {
				player.redSnake.deselect();
			}
			player.redSnake = null;

			spawnShooters();
			spawnBouncers();
			spawnMinions();
			toggleTargetLabels(false);

			if (player.currentEdge) {
				player.currentEdge.destroy();
				player.currentEdge = null;
			}
			if (player.firstNode) {
				player.firstNode.deselect();
				player.firstNode = null;
			}

		},1000)
	},
	playBoss = function() {
		if (!bossMode) {
			bossMode = true;
			// lock gold snakes
			for (var i = level.layers.length - 1; i >= 0; i--) {
				var layer = level.layers[i];
				if (layer.name === "nodes") {
					nodes = layer.objects.filter(function(x) {
						return x.type === "node";
					});
					for (var j = 0; j < nodes.length; j++) {
						var obj = nodes[j];
						var color = 3; // gold for boss snakes
						if (obj.name === "goldNode") {
							// create gold nodes
							var node = Crafty.e('Node')
								._Node(obj.x + 10, obj.y +10 , 0, color); // 0 is green, 1 is red, 2 is balck, 3 is gold, 4 is 'selected', 5 is target
							if (obj.properties && typeof obj.properties.goldSnake !== 'undefined') {
								var first = goldPairs[obj.properties.goldSnake];
								if(!!first && first.constructor == Array) {
									first.push(node);
								} else {
									goldPairs[obj.properties.goldSnake] = [node];
								}
							} else {
								goldHoles.push(node);
							}
						}
					}
					for (var j = goldPairs.length - 1; j >= 0; j--) {
						var goldPair = goldPairs[j];
						var goldSnake = Crafty.e('aEdge')._aEdge(goldPair[0], goldPair[1]);
						goldPair[0].snake = goldSnake;
						goldPair[1].snake = goldSnake;
					}
				}
			}
			// destroy all old enemies
			var enemies = ['Minion', 'Bouncer', 'Shooter'];
			for (var i = enemies.length - 1; i >= 0; i--) {
				var enms = Crafty(enemies[i]);
				for (var j = enms.length - 1; j >= 0; j--) {
					Crafty(enms[j]).destroy();
				}
			}
			// spawn new enemies
			spawnMinions();
			spawnBouncers();
			spawnShooters();
		}
	}

	buildLevel();
});


