// LOADING SCENE
Crafty.scene('Loading', function() {
	// // Draw some text for the player to see in case the file
	// //  takes a noticeable amount of time to load

	//Crafty.background('url(assets/images/background1.png)');
     // Crafty.background(mycolors.background);
    Crafty.background('url(assets/images/outside2.png)');
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 200,
			y: 150,
			w: 400,
			h: 200,
			z: 8
		})
		.textFont({
			size: '34px',
			family: fontFamily1
		})
		.css({			
			'background-color': mycolors.textBlockBg,
			'border': ('2px dashed' + mycolors.button1),
			'border-radius': '8px',
			'cursor': 'pointer',
			'text-align': 'center',
			'padding-top': '3px',
			'color':mycolors.textBlockText,
		})
		.text('<BR><br>Loading,<br><BR> please wait...');
	Crafty.load(assetsObject,
		function() {
			//when loaded
			console.log('everything loaded!');
			 
			Crafty.sprite(20, 20, "assets/images/alphabet.png", {
				letterA: [0,0],				
			});	

			Crafty.sprite(20, 20, "assets/images/fireball.png", {
				fireball: [0,0],				
			});	

			Crafty.sprite(20, 20, "assets/images/snakeHead.png", {
				snakeHead: [0,0],				
			});	

			Crafty.sprite(40, 40, "assets/images/tiles.png", {
				tiles: [0,0],				
			});

			Crafty.sprite(35, 35, "assets/images/player.png", {
				playerSprite: [0,0],				
			});

			Crafty.sprite(35, 35, "assets/images/scepter.png", {
				scepter: [0,0],				
			});

			Crafty.sprite(35, 35, "assets/images/hair.png", {
				hair: [0,0],				
			});

			Crafty.sprite(35, 35, "assets/images/crown.png", {
				crown: [0,0],				
			});

			Crafty.sprite(35, 35, "assets/images/amulet.png", {
				amulet: [0,0],				
			});

			Crafty.sprite(40, 40, "assets/images/bat.png", {
				bat: [0,0],				
			});

			Crafty.sprite(40, 40, "assets/images/scorpion.png", {
				scorpion: [0,0],				
			});

			Crafty.sprite(40, 40, "assets/images/scarab.png", {
				scarab: [0,0],				
			});

			Crafty.sprite(40, 80, "assets/images/statue.png", {
				statue: [0,0],				
			});

			Crafty.sprite(40, 40, "assets/images/saveTorch.png", {
				saveTorch: [0,0],				
			});
			//bgMusic = Crafty.audio.play('bgmusic', -1, 0.3);
			levelIdx = 0;
			Crafty.scene('Main');
			// Crafty.scene('StartScreen');
		},

		function(e) { // onProgress
			//console.log(e);   // uncomment to see what has been loaded
		},

		function(e) {
			console.log('loading error');
			console.log(e);
		}
	);
    
    
    

});